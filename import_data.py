import os

from os import listdir
from os.path import isfile, join
INPUT = DIR = os.getcwd()+"/TestPython/data/"
onlydirs = [DIR+str(f) for f in listdir(DIR) if not isfile(join(DIR, f))]

def execute(req):
    pass

for dr in onlydirs:
    onlyfiles = [dr+"/"+str(f) for f in listdir(dr) if isfile(join(dr, f))]
    for f in onlyfiles:
        with open(f,'r') as f:
            for req in f:
                if len(req)>5:
                    execute(req);