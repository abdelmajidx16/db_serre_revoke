meta = {
    'eacta.data.gain.recolte': dict(name='composant_farm_ids', type='Many2many', comodel_name="eacta.plantation.serre",
                                    relation="eacta_r_gain_serre",
                                    column1="eacta_data_gain_recolte_id",
                                    column2="composant_farm_id", string="Serres",
                                    required=True
                                    ),

    'eacta.data.charge.consomable': dict(name='composant_farm_ids', type='Many2many',
                                         comodel_name="eacta.plantation.serre",
                                         relation="eacta_r_consomable_serre",
                                         column1="charges_consomable_id",
                                         column2="composant_farm_id", string="Serres",
                                         required=True
                                         ),

    'eacta.data.charge.materiel': dict(name='composant_farm_ids', type='Many2many',
                                       comodel_name="eacta.plantation.serre",
                                       relation="eacta_r_materiel_serre",
                                       column1="eacta_data_charge_materiel_id",
                                       column2="composant_farm_id", string="Serres",
                                       required=True
                                       ),

    'eacta.data.charge.travail': dict(name='composant_farm_ids', type='Many2many',
                                      comodel_name="eacta.plantation.serre",
                                      relation="eacta_r_travail_serre",
                                      column1="eacta_data_charge_travail_id",
                                      column2="composant_farm_id", string="Serres",
                                      required=True
                                      ),

    'eacta.r.variete.serre.period': dict(name='composant_farm_ids', type='One2many',
                                         comodel_name="eacta.plantation.serre",
                                         inverse_name="plantation_id",
                                         string=str("Serres"),
                                         required=False, ),
    'eacta.qualite.echantillon': dict(name='serre_id', type='Many2one', comodel_name="eacta.plantation.serre",
                                      string=str("Serre"), required=True, ),
    'eacta.diviser.serre': dict(name='serres_ids', type='One2many', comodel_name="eacta.plantation.serre",
                                inverse_name="divise_id",
                                string=str("Serres"), required=False, ),
    'eacta.historique.produit': dict(name='composant_farm_ids', type='Many2many', comodel_name="eacta.plantation.serre",
                                     relation="eacta_r_historique_serre",
                                     column1="historique_id",
                                     column2="composant_farm_id", string="Serres",
                                     ),
    'eacta.diviser.serre': dict(name='serre_id', type='Many2one', comodel_name="eacta.plantation.serre",
                                string=str("Serre"), required=True, ),
    'eacta.farm.qualite.serre.products': dict(name='serre_id', type='Many2one', comodel_name="eacta.plantation.serre",
                                              string=str("Dernier Traitement"),
                                              required=True),
    'eacta.data.recolte.qualite': dict(name='serre_id', type='Many2one', comodel_name="eacta.plantation.serre",
                                       string=str("Serres"),
                                       required=True),
    'eacta.programme.fertigation': dict(name='composant_farm_ids', type='Many2many',
                                        comodel_name="eacta.plantation.serre",
                                        relation="eacta_r_programme_plantation_serre",
                                        column1="eacta_programme_fertigation_id",
                                        column2="composant_farm_id", string="Serres",
                                        ),
    'eacta.farm.qualite.traitement.serre': dict(name='serre_ids', type='Many2many',
                                                comodel_name="eacta.plantation.serre",
                                                relation="eacta_r_traitement_serre_serre",
                                                column1="traitement_serre_id",
                                                column2="serre_id", string="Serres",
                                                ),
    'eacta.farm.qualite.traitement': dict(name='serres_ids', type='Many2many', comodel_name="eacta.plantation.serre",
                                          relation="eacta_r_qualite_traitement_serre",
                                          column1="eacta_qualite_traitement_id",
                                          column2="composant_farm_id", string="Serres",
                                          required=True
                                          ),
    'eacta.fiche.desinfection': dict(name='serres_ids', type='Many2many', comodel_name="eacta.plantation.serre",
                                     relation="eacta_r_fiche_desinfection_serre",
                                     column1="fiche_desinfection_id",
                                     column2="serre_id", string=str(u"Serres et superficie desinfectees"),
                                     ),
    'eacta.suivi.fertigation': dict(name='composant_farm_ids', type='Many2many', comodel_name="eacta.plantation.serre",
                                    relation="eacta_r_suivi_fertigation_serre",
                                    column1="eacta_suivi_fertigation_id",
                                    column2="composant_farm_id", string="Serres",
                                    ),
    # 'consomation.report': dict(name='composant_id', type='Many2one', comodel_name="eacta.plantation.serre",
    #                            string=str("Composant Ferme"),
    #                            readonly=True, ),
    # 'charge.travail.report': dict(name='composant_id', type='Many2one', comodel_name="eacta.plantation.serre",
    #                               string=str("Composant Ferme"),
    #                               readonly=True, ),
    # 'materiel.report': dict(name='composant_id', type='Many2one', comodel_name="eacta.plantation.serre",
    #                         string=str("Composant Ferme"),
    #                         readonly=True, ),
    # 'recolte.report': dict(name='composant_id', type='Many2one',
    #                        comodel_name="eacta.plantation.serre", string=str("Composant Ferme"),
    #                        readonly=True, ),

}

# -------------------
import psycopg2
from psycopg2 import extras
from datetime import datetime,date
import pickle
import os




def connect(DB, USER, PASS):
    conn = psycopg2.connect(dbname=DB,
                            user=USER,
                            password=PASS,
                            host='localhost',
                            port=5432)
    return conn, conn.cursor

def count_missing(id_obj):
    global DIR
    global PASS
    global DB
    global USER
    req_mone = """
                SELECT *
                FROM %s 
                WHERE %s=%d
                """
    req_to_revert_mone = "UPDATE %s SET %s=%s WHERE id=%s "
    req_mmany = """
                    SELECT *
                    FROM %s 
                    WHERE %s=%d
                    """
    req_to_revert_m2m = "INSERT INTO %s(%s) values(%s)\n"

    conn, cursor = connect(DB, USER, PASS)
    cur = cursor(cursor_factory=extras.DictCursor)
    cur.execute("SELECT * FROM eacta_plantation_serre WHERE id=%d"%id_obj)
    obj_del = cur.fetchall()
    if len(obj_del):
        with open(DIR + "deleted_serre/deleted_serre.txt", mode="wb") as f:
            for l in obj_del:
                kys = ",".join(list(l.keys()))
                vls = []
                for j in l.keys():
                    i = l[j]
                    if isinstance(i, datetime):
                        i=i.strftime("%Y-%m-%d %H:%M:%S")
                    elif isinstance(i,date):
                        i=i.strftime("%Y-%m-%d")
                    elif i is None:
                        i='null'
                    vls.append(i)
                vls = str(vls)[1:-1]
                f.write(req_to_revert_m2m % ('eacta_plantation_serre', kys, vls))
    for model, info in meta.items():
        print("---- MODEL :",model,"-----")
        conn,cursor = connect(DB, USER, PASS)
        #cur = cursor(cursor_factory=extras.NamedTupleCursor)
        cur = cursor(cursor_factory=extras.DictCursor)
        lines = []
        table = model.lower().replace('.','_')
        co_table = info['comodel_name'].lower().replace(".","_")
        field_name = info['name']
        #------- Many2One ------
        if info['type'].lower() == 'many2one':
            cur.execute(req_mone %(table, field_name, id_obj))
            lines = cur.fetchall()
            print("MO:The %s existe %d time in %s"%(co_table, len(lines), table))
        print("--- Lines ---")
        if len(lines) > 0:
            with open(DIR + "M2One/" + table + ".txt", mode="wb") as f:
                for l in lines:
                    f.write(req_to_revert_mone % (table, field_name, l[field_name], l['id']) + "\n")

        #------ Many2Many --------

        lines = []
        table = ''
        co_table = ''
        field_name = ''
        if info['type'].lower() == 'many2many':
            table = info['relation'].lower().replace('.', '_')
            co_table = info['comodel_name'].lower().replace(".", "_")
            field_name = info['column2']
            cur.execute(req_mmany %(table, field_name, id_obj))
            lines = cur.fetchall()
            print("MM:The %s existe %d time in %s"%(co_table, len(lines), table))
        print("--- Lines ---")
        if len(lines) > 0:
            with open(DIR + "M2M/" + table + ".txt", mode="wb") as f:
                for l in lines:
                    kys = ",".join(list(l.keys()))
                    vls=[l[i] for i in l.keys()]
                    vls = str(vls)[1:-1]
                    f.write(req_to_revert_m2m % (table, kys, vls))



from os import listdir
from os.path import isfile, join

def import_data():
    global DIR
    global DB
    global USER
    global PASS
    onlydirs = [DIR + str(f) for f in listdir(DIR) if not isfile(join(DIR, f))]
    onlydirs.sort(reverse=True)
    conn, cursor = connect(DB, USER, PASS)
    cur = cursor(cursor_factory=extras.DictCursor)

    for dr in onlydirs:
        onlyfiles = [dr + "/" + str(f) for f in listdir(dr) if isfile(join(dr, f))]
        print("====",dr)
        for f in onlyfiles:
            print("----",f)
            with open(f, 'r') as f:
                for req in f:
                    if len(req) > 5:
                        cur.execute(req)
        conn.commit()
    conn.close()

# ----- MAIN ---
DIR = "./data/"
DB = 'DB_DOMAINE_2703'
USER = 'odoo8'
PASS = 'admin'
print("=== Missing count")

#count_missing(1150)
print("---- Data to DB ---")
import_data()
print("--- done --")
